const keyboard_cat_1 = [
    [
        {
            text: 'Tide - 398.99₽',
            callback_data: 'cat_1_1'
        },
    ],
    [
        {
            text: 'Ariel pods - 683.99₽',
            callback_data: 'cat_1_2'
        },
    ],
    [
        {
            text: 'Fairy - 97.99₽',
            callback_data: 'cat_1_3'
        },
    ],
    [
        {
            text: 'Domestos - 226.99₽',
            callback_data: 'cat_1_4'
        },
    ],
    [
        {
            text: 'Finish - 1894.99₽',
            callback_data: 'cat_1_1'
        },
    ],
    [
        {
            text: 'В каталог',
            callback_data: 'catalogue'
        },
    ],
    [
        {
            text: 'В корзину',
            callback_data: 'cart'
        }
    ]

]

module.exports = {
    keyboard_cat_1
}