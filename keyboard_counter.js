const keyboard_counter = [
    [
        {
            text: '+1',
            callback_data: 'add'
        },
        {
            text: '-1',
            callback_data: 'reduce'
        }
    ],
    [
        {
            text: 'В каталог',
            callback_data: 'catalogue'
        },
    ],
    [
        {
            text: 'В корзину',
            callback_data: 'cart'
        }
    ]

]

module.exports = {
    keyboard_counter
}