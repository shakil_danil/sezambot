const keyboard_catalogue = [
    [
        {
            text: 'Бытовая химия',
            callback_data: 'cat_1'
        },
    ],
    [
        {
            text: 'Газированные напитки',
            callback_data: 'cat_2'
        },
    ],
    [
        {
            text: 'Гигиена',
            callback_data: 'cat_3'
        },
    ],
    [
        {
            text: 'Косметика для женщин',
            callback_data: 'cat_4'
        },
    ],
    [
        {
            text: 'Консервы',
            callback_data: 'cat_5'
        },
    ],
    [
        {
            text: 'Крупы',
            callback_data: 'cat_6'
        },
    ],
    [
        {
            text: 'Молочные продукты',
            callback_data: 'cat_7'
        },
    ],
    [
        {
            text: 'Косметика для мужчин',
            callback_data: 'cat_8'
        },
    ],
    [
        {
            text: 'Мясо, яйца',
            callback_data: 'cat_9'
        },
    ],
    [
        {
            text: 'Фрукты и овощи',
            callback_data: 'cat_10'
        },
    ],
    [
        {
            text: 'Хлеб',
            callback_data: 'cat_11'
        },
    ],
    [
        {
            text: 'В корзину',
            callback_data: 'cart'
        }
    ]
];


module.exports = {
    keyboard_catalogue
}