const keyboard_menu = [
    [
      {
        text: 'Каталог товаров',
        callback_data: 'catalogue'
      },
    ],
    [
        {
          text: 'Корзина',
          callback_data: 'cart'
        }
    ],
    [
        {
            text: 'Задать вопрос',
            callback_data: 'question'
        }
    ]
  ];

  module.exports = {
    keyboard_menu
}