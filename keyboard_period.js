const keyboard_period = [
    [
      {
        text: 'Раз в 2 дня',
        callback_data: 'p1'
      },
    ],
    [
        {
          text: 'Раз в 1 неделю',
          callback_data: 'p2'
        }
    ],
    [
        {
            text: 'Раз в 2 недели',
            callback_data: 'p3'
        }
    ],
    [
        {
            text: 'Раз в месяц',
            callback_data: 'p4'
        }
    ],
    [
        {
            text: 'В каталог',
            callback_data: 'catalogue'
        }
    ]
  ];

  module.exports = {
    keyboard_period
}